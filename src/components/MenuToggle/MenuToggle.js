import React from "react";
import classes from './MenuToggle.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';

const MenuToggle = props => {
    const cls = [
        classes.MenuToggle,
    ];

    let faIcon = faBars;

    if (props.isOpen) {
        faIcon = faTimes;
        cls.push(classes.open)
    }

    return(
        <FontAwesomeIcon
            className={cls.join(' ')}
            icon={faIcon}
            onClick={props.onToggle}
        >
        </FontAwesomeIcon>
    )
};

export default MenuToggle;