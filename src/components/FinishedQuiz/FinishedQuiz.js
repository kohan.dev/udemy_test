import React from "react";
import classes from './FinishedQuiz.module.css';
import Button from "../Button/Button";

const FinishQuiz = props => {
    let successCount = Object.keys(props.results).reduce((total, key) =>{
        if (props.results[key] === 'success') {
            total++;
        }
        return total;
    }, 0);

    return (
        <div className={classes.FinishedQuiz}>
            <ul>
                { props.quiz.map((quizItem, index) => {
                    console.info('quizItem.id: ',props.results[quizItem.id-1]);
                    return (
                        <li
                            key={index}
                        >
                            <strong>{index + 1}.</strong>&nbsp;
                            {quizItem.question}
                            <span className={classes[props.results[quizItem.id-1]]}>O</span>
                        </li>
                    )
                }) }
            </ul>

            <p>правильно: {successCount} из {props.quiz.length}</p>
            <div>
                <Button onClick={props.onRetry} type={'primary'}>повторить</Button>
                <Button type={'success'}>Перейти к списку тестов</Button>
            </div>

        </div>
    )
};

export default FinishQuiz;