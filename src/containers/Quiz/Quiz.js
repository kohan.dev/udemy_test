import React, {Component} from "react";
import classes from './Quiz.module.css';
import ActiveQuiz from "../../components/ActiveQuiz/ActiveQuiz";
import FinishQuiz from "../../components/FinishedQuiz/FinishedQuiz";

class Quiz extends Component {
    state = {
        results: {}, //{ [id]: success error }
        isFinished: false,
        activeQuestion: 0,
        answerState: null, //{ [id]: success error }
        quiz: [
            {   id: 1,
                question: 'Сколько в таблице Пифагора чисел?',
                rightAnswerId: 3,
                answers: [
                    {text: '100', id: 1},
                    {text: '81', id: 2},
                    {text: '99', id: 3},
                    {text: '89', id: 4},
                ]
            },
            {   id: 2,
                question: 'Чему равно ускорение свободного падения?',
                rightAnswerId: 4,
                answers: [
                    {text: '10 м/с2', id: 1},
                    {text: '9,5 м/с2', id: 2},
                    {text: '8,9 м/с2', id: 3},
                    {text: '9,8 м/с2', id: 4},
                ]
            },
            {   id: 3,
                question: 'Чему равна плотность воды?',
                rightAnswerId: 2,
                answers: [
                    {text: '1,1 кг/дм3', id: 1},
                    {text: '1,0 кг/дм3', id: 2},
                    {text: '9,9 кг/дм3', id: 3},
                    {text: '9,8,0 кг/дм3', id: 4},
                ]
            },
            {   id: 4,
                question: 'Что не является углеродом?',
                rightAnswerId: 2,
                answers: [
                    {text: 'уголь', id: 1},
                    {text: 'сапфир', id: 2},
                    {text: 'грифель', id: 3},
                    {text: 'алмаз', id: 4},
                ]
            },
            ],

    };

    onAnswerClickHandler = answerId => {
        if (this.state.answerState) {
            const key = Object.keys(this.state.answerState)[0];
            if (this.state.answerState[key] === 'success') {
                return void 0;
            }
        }

        const qestion = this.state.quiz[this.state.activeQuestion];
        const results = this.state.results;

        if (qestion.rightAnswerId === answerId) {
            console.info('answerId ', answerId);
            if (!results[this.state.activeQuestion]) {
                results[this.state.activeQuestion] ='success'
            }
            this.setState({
                answerState: {[answerId]: 'success'},
                results: results,
            });

            const timeout = window.setTimeout(() => {
                if (this.isQuizFinished()) {
                    this.setState({
                        isFinished: true,
                    })
                } else {
                    this.setState({
                        activeQuestion: this.state.activeQuestion +1,
                        answerState: null,
                    })
                }
                window.clearTimeout(timeout)
            }, 500);

        } else {
            results[this.state.activeQuestion] = 'error';
            this.setState({
                answerState: {[answerId]: 'error'},
                results: results,
            });
        }
    console.info('results: ', this.state.results);

    };

    isQuizFinished() {
        return this.state.activeQuestion + 1 === this.state.quiz.length
    }

    RetryHandler = () => {
        this.setState({
            results: {},
            isFinished: false,
            activeQuestion: 0,
            answerState: null,
        })
    };

    render() {
        return (
            <div className={classes.Quiz}>
                <div className={classes.QuizWrapper}>
                    <h1>ОТВЕТИТЬ НА ВОПРОСЫ</h1>
                    {
                        this.state.isFinished
                        ? <FinishQuiz
                                results={this.state.results}
                                quiz={this.state.quiz}
                                onRetry={this.RetryHandler}
                            />
                        :<ActiveQuiz
                            answers={this.state.quiz[this.state.activeQuestion].answers}
                            question={this.state.quiz[this.state.activeQuestion].question}
                            onAnswerClick={this.onAnswerClickHandler}
                            quizLength={this.state.quiz.length}
                            answerNumber={this.state.activeQuestion + 1}
                            state={this.state.answerState}
                        />
                    }

                </div>
            </div>
        );
    }
}

export default Quiz;
